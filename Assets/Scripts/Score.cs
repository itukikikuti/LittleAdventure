﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStack;

public class Score : SingletonPrefab<Score>
{
    [SerializeField] private AudioClip coinSound = null;
    [SerializeField] private GameObject additivePointPrefab = null;

    public void Add(int point, Vector3 position)
    {
        Audio.Play(coinSound);
        GameObject clone = Instantiate(additivePointPrefab, position, Quaternion.identity);
        clone.GetComponent<AdditivePoint>().Setup(point);
    }
}
