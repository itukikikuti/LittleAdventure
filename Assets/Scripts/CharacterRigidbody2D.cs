﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GameStack;

[RequireComponent(typeof(BoxCollider2D))]
public class CharacterRigidbody2D : MonoBehaviour
{
    [SerializeField] public float slopeLimit = 45f;
    [SerializeField] public float maximumMoveSpeed = 1f;
    [SerializeField, NotEditable] public bool isGrounded = false;
    [SerializeField, NotEditable] public bool isLeftToutched = false;
    [SerializeField, NotEditable] public bool isRightToutched = false;
    [HideInInspector] public new Rigidbody2D rigidbody = null;
    [HideInInspector] public BoxCollider2D boxCollider = null;
    private ContactPoint2D[] contacts = new ContactPoint2D[0];
    private RaycastHit2D ground;
    private float jumpForce = 0f;
    //[SerializeField] private Lazy<Rigidbody2D> rb = new Lazy<Rigidbody2D>();

    public Vector2 Velocity
    {
        get => rigidbody.velocity;
        set => rigidbody.velocity = value;
    }

    public float HorizontalVelocity { get; private set; } = 0f;

    private void Awake()
    {
        rigidbody = gameObject.AddComponent<Rigidbody2D>();
        //rigidbody.hideFlags = HideFlags.HideInInspector;
        rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        rigidbody.sharedMaterial = new PhysicsMaterial2D { bounciness = 0f, friction = 0f };
        rigidbody.interpolation = RigidbodyInterpolation2D.Extrapolate;

        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void FixedUpdate()
    {
        List<ContactPoint2D> temp = new List<ContactPoint2D>();
        rigidbody.GetContacts(temp);
        contacts = temp.ToArray();

        isLeftToutched = false;
        isRightToutched = false;
        foreach (var contact in contacts)
        {
            if (contact.normal.x >= Mathf.Sin(slopeLimit * Mathf.Deg2Rad))
                isLeftToutched = true;

            if (contact.normal.x <= -Mathf.Sin(slopeLimit * Mathf.Deg2Rad))
                isRightToutched = true;
        }

        Velocity = new Vector2(Mathf.Clamp(Velocity.x, -maximumMoveSpeed, maximumMoveSpeed), Velocity.y);

        if (isLeftToutched)
            HorizontalVelocity = Mathf.Max(0f, HorizontalVelocity);

        if (isRightToutched)
            HorizontalVelocity = Mathf.Min(0f, HorizontalVelocity);

        if (isGrounded)
        {
            float size = boxCollider.bounds.extents.x * 2f;
            float extents = boxCollider.bounds.extents.x;
            Vector2 origin = (Vector2)boxCollider.bounds.center + Vector2.down * (boxCollider.bounds.extents.y - extents);
            RaycastHit2D[] hits = VisiblePhysics2D.BoxCastAll(origin, new Vector2(size, size), 0f, Vector2.down, Mathf.Infinity, Physics2D.GetLayerCollisionMask(gameObject.layer)).Where(hit => hit.transform != transform).OrderBy(hit => hit.distance).ToArray();

            bool hitGround = false;
            foreach (var hit in hits)
            {
                if (hit.distance < 0.1f && hit.normal.y > Mathf.Cos(slopeLimit * Mathf.Deg2Rad))
                {
                    ground = hit;
                    rigidbody.position = hit.centroid + Vector2.up * (boxCollider.bounds.extents.y - extents + hit.distance) - boxCollider.offset;
                    hitGround = true;
                    break;
                }
            }

            if (!hitGround)
            {
                isGrounded = false;
                rigidbody.gravityScale = 1f;
                return;
            }

            HorizontalVelocity = Mathf.Clamp(HorizontalVelocity, -maximumMoveSpeed, maximumMoveSpeed);
            HorizontalVelocity = Mathf.MoveTowards(HorizontalVelocity, 0f, 0.1f);

            Vector2 groundLeftDirection = new Vector3(ground.normal.y, -ground.normal.x);
            if ((groundLeftDirection * HorizontalVelocity).y > 0f)
            {
                float projectedSpeed = Vector3.ProjectOnPlane(groundLeftDirection * HorizontalVelocity, Vector2.up).x;
                Velocity = groundLeftDirection * projectedSpeed;
            }
            else
            {
                Velocity = groundLeftDirection * HorizontalVelocity;
            }

            if (ground.rigidbody.bodyType == RigidbodyType2D.Dynamic)
            {
                Velocity += ground.rigidbody.GetPointVelocity(ground.point);
            }

            if (jumpForce > 0f)
            {
                Velocity = new Vector2(Velocity.x, jumpForce);
                jumpForce = 0f;
                isGrounded = false;
                rigidbody.gravityScale = 1f;
                rigidbody.position += new Vector2(0f, 0.1f);
            }
        }
        else
        {
            //Velocity = new Vector2(HorizontalVelocity, Velocity.y);
            HorizontalVelocity = Velocity.x;
            CheckGrounding();
        }
    }

    private void CheckGrounding()
    {
        isGrounded = false;
        foreach (var contact in contacts)
        {
            if (contact.normal.y > Mathf.Cos(slopeLimit * Mathf.Deg2Rad))
            {
                isGrounded = true;
                rigidbody.gravityScale = 0f;
                return;
            }
        }
    }

    public void Move(float force)
    {
        if (isGrounded)
        {
            HorizontalVelocity += force;
        }
        else
        {
            Velocity += new Vector2(force, 0f);
        }
    }

    public void Jump(float force)
    {
        jumpForce = force;
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying) return;

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + (Vector3)Velocity);

        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, transform.position + new Vector3(HorizontalVelocity, 0f, 0f));

        foreach (var contact in contacts)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(contact.point, 0.05f);
            Gizmos.DrawLine(contact.point, contact.point + contact.normal * 0.3f);

            if (contact.normal.y > Mathf.Cos(slopeLimit * Mathf.Deg2Rad))
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(contact.point - Vector2.right * 0.15f, contact.point - Vector2.left * 0.15f);
            }

            if (contact.normal.x >= Mathf.Sin(slopeLimit * Mathf.Deg2Rad) ||
                contact.normal.x <= -Mathf.Sin(slopeLimit * Mathf.Deg2Rad))
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(contact.point - Vector2.up * 0.15f, contact.point - Vector2.down * 0.15f);
            }
        }
    }
}
