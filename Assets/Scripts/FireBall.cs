﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterRigidbody2D))]
public class FireBall : MonoBehaviour
{
    private CharacterRigidbody2D characterBody = null;
    public float speed;

    private void Start()
    {
        characterBody = GetComponent<CharacterRigidbody2D>();
        characterBody.rigidbody.gravityScale = 2f;
    }

    private void Update()
    {
        characterBody.Move(speed);

        if (characterBody.isGrounded)
        {
            characterBody.Jump(13f);
        }

        if (characterBody.isLeftToutched || characterBody.isRightToutched)
        {
            GetComponent<ParticleSystem>().Stop();
            Destroy(this);
            Destroy(gameObject, 0.1f);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            Destroy(collision.gameObject);

            GetComponent<ParticleSystem>().Stop();
            Destroy(this);
            Destroy(gameObject, 0.1f);
        }
    }
}
