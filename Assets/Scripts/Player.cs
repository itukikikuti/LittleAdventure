﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GameStack;

[RequireComponent(typeof(CharacterRigidbody2D))]
public class Player : MonoBehaviour
{
    public enum State
    {
        Mini,
        Big,
        Fire,
    }

    enum Direction
    {
        Right,
        Left,
    }

    [SerializeField] Transform model = null;
    [SerializeField] Animator animator = null;
    [SerializeField] float jump;
    [SerializeField] AudioClip jumpSound = null;
    [SerializeField] AudioClip fireSound = null;
    private CharacterRigidbody2D characterBody = null;
    private AudioSource audioSource = null;
    [SerializeField, NotEditable] private float targetAngle;
    [SerializeField] private Vector3 scaleForce;
    [SerializeField] private Vector3 scaleTarget;
    [SerializeField] private float scaleSpring;
    [SerializeField] private float scaleRepulsion;
    [SerializeField] private ParticleSystem fireEffect = null;
    [SerializeField] private GameObject fireBallPrefab = null;
    [SerializeField] public State state = State.Mini;
    [SerializeField, HideInInspector] private Direction direction = Direction.Right;
    [SerializeField] private int fireBallCount = 0;

    private void Start()
    {
        characterBody = GetComponent<CharacterRigidbody2D>();
        characterBody.rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = jumpSound;

        targetAngle = model.eulerAngles.y;
        UpdateScale();
    }

    private void Update()
    {
        if (characterBody.Velocity.y <= 0f)
        {
            audioSource.volume = Mathf.MoveTowards(audioSource.volume, 0f, 5f * Time.deltaTime);
        }
    }

    private void FixedUpdate()
    {
        if (characterBody.isGrounded)
            characterBody.Move(Input.GetAxis("Horizontal") * 0.5f);
        else
            characterBody.Move(Input.GetAxis("Horizontal") * 0.1f);

        if (characterBody.HorizontalVelocity < -0.1f)
        {
            direction = Direction.Left;
            targetAngle = 225f;
        }
        if (characterBody.HorizontalVelocity > 0.1f)
        {
            direction = Direction.Right;
            targetAngle = 135f;
        }

        model.eulerAngles = new Vector3(0f, Mathf.MoveTowards(model.eulerAngles.y, targetAngle, 30f), 0f);

        animator.SetFloat("speed", Mathf.Max(0.1f, Mathf.Abs(characterBody.HorizontalVelocity)));
        animator.SetBool("isGrounded", characterBody.isGrounded);

        if (Input.GetKey(KeyCode.X))
            characterBody.maximumMoveSpeed = 10f;
        else
            characterBody.maximumMoveSpeed = 5f;

        if (state == State.Fire && Input.GetKeyDown(KeyCode.X) && fireBallCount < 2)
        {
            GameObject clone = Instantiate(fireBallPrefab, transform.position, Quaternion.identity);
            clone.GetComponent<FireBall>().speed = direction == Direction.Right ? 5f : -5f;
            clone.OnDestroy().Subscribe(_ => fireBallCount--);
            fireBallCount++;
            Audio.Play(fireSound);
        }

        if (Input.GetKey(KeyCode.LeftArrow) && characterBody.isLeftToutched && characterBody.Velocity.y < 0f)
            characterBody.Velocity = new Vector2(characterBody.Velocity.x, characterBody.Velocity.y * 0.9f);

        if (Input.GetKey(KeyCode.RightArrow) && characterBody.isRightToutched && characterBody.Velocity.y < 0f)
            characterBody.Velocity = new Vector2(characterBody.Velocity.x, characterBody.Velocity.y * 0.9f);

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (characterBody.isGrounded)
            {
                characterBody.Jump(jump);
                audioSource.volume = 1f;
                audioSource.Play();
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftArrow) && characterBody.isLeftToutched)
                {
                    characterBody.Velocity = new Vector2(5f, jump);
                    audioSource.volume = 1f;
                    audioSource.Play();
                }

                if (Input.GetKey(KeyCode.RightArrow) && characterBody.isRightToutched)
                {
                    characterBody.Velocity = new Vector2(-5f, jump);
                    audioSource.volume = 1f;
                    audioSource.Play();
                }
            }
        }

        if (characterBody.Velocity.y > 0f && Input.GetKeyUp(KeyCode.Z))
        {
            characterBody.Velocity *= new Vector2(1f, 0.5f);
        }
    }

    public async void Big()
    {
        Time.timeScale = 0f;
        scaleForce = new Vector3(-1f, 1f, -1f);
        scaleTarget = new Vector3(2f, 2f, 2f);

        await new WaitForSecondsRealtime(1f);
        characterBody.boxCollider.offset = new Vector2(0f, 0.45f);
        characterBody.boxCollider.size = new Vector2(0.7f, 1.9f);
        Time.timeScale = 1f;

        state = State.Big;
    }

    public async void Fire()
    {
        Time.timeScale = 0f;
        if (state == State.Mini)
        {
            scaleForce = new Vector3(-1f, 1f, -1f);
            scaleTarget = new Vector3(2f, 2f, 2f);
        }
        fireEffect.Play();

        await new WaitForSecondsRealtime(1f);
        characterBody.boxCollider.offset = new Vector2(0f, 0.45f);
        characterBody.boxCollider.size = new Vector2(0.7f, 1.9f);
        Time.timeScale = 1f;

        state = State.Fire;
    }

    private async void UpdateScale()
    {
        while (true)
        {
            scaleForce += (scaleTarget - model.localScale) * scaleSpring;
            scaleForce *= scaleRepulsion;
            model.localScale += scaleForce;
            //model.localScale = Vector3.Min(model.localScale, scaleTarget + new Vector3(limit, limit, limit));
            //model.localScale = Vector3.Max(model.localScale, scaleTarget - new Vector3(limit, limit, limit));

            await new WaitForNextFrame();
        }
    }
}
