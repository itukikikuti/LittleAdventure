﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStack;

public class LuckyBlock : MonoBehaviour
{
    [SerializeField] private GameObject itemPrefab = null;
    [SerializeField] private GameObject kinokoPrefab = null;
    [SerializeField] private Sprite openedSprite = null;
    [SerializeField] private GameObject graphic = null;
    [SerializeField, NotEditable] private bool isOpened = false;

    private async void Open(Collider2D collider)
    {
        if (isOpened) return;
        isOpened = true;

        graphic.GetComponent<SpriteRenderer>().sprite = openedSprite;

        Player player = collider.GetComponent<Player>();

        if (itemPrefab != null)
        {
            GameObject clone = null;

            if (player.state == Player.State.Mini)
                clone = Instantiate(kinokoPrefab, transform.position, Quaternion.identity);
            else
                clone = Instantiate(itemPrefab, transform.position, Quaternion.identity);

            _ = clone.transform.TweenMoveDelta(Vector3.up, 1f);
        }
        else
        {
            Score.Instance.Add(1, transform.position + new Vector3(0f, 0.5f, 0f));
        }

        _ = graphic.transform.TweenMoveLocal(new Vector3(0f, 0.15f, -0.5f), 0.1f);
        _ = graphic.transform.TweenScaleLocal(new Vector3(1.15f, 1.15f, 1.15f), 0.1f);
        await new WaitForSeconds(0.1f);
        _ = graphic.transform.TweenMoveLocal(new Vector3(0f, 0f, -0.5f), 0.1f);
        _ = graphic.transform.TweenScaleLocal(Vector3.one, 0.1f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        foreach (var contact in collision.contacts)
        {
            if (contact.collider.gameObject.layer == LayerMask.NameToLayer("Player") &&
                contact.normal.y > Mathf.Cos(45f * Mathf.Deg2Rad))
            {
                Open(contact.collider);
            }
        }
    }
}
