﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterRigidbody2D))]
public class Kinoko : MonoBehaviour
{
    private CharacterRigidbody2D characterBody = null;
    [SerializeField, HideInInspector] private float speed = 0.5f;

    private void Start()
    {
        characterBody = GetComponent<CharacterRigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (characterBody.isLeftToutched) speed = Mathf.Abs(speed);
        if (characterBody.isRightToutched) speed = -Mathf.Abs(speed);

        characterBody.Move(speed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            collision.gameObject.GetComponent<Player>().Big();
            Destroy(gameObject);
        }
    }
}
