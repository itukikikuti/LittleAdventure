﻿using System.Linq;
using UnityEngine;
using GameStack;

[RequireComponent(typeof(CapsuleCollider))]
public class CharacterRigidbody : MonoBehaviour
{
    [SerializeField] public float slopeLimit = 45f;
    [SerializeField] public float maximumMoveSpeed = 1f;
    [SerializeField, NotEditable] public bool isGrounded = false;
    [HideInInspector] public new Rigidbody rigidbody = null;
    [HideInInspector] public CapsuleCollider capsuleCollider = null;

    public Vector3 Velocity
    {
        get;
        set;
    }

    private void Start()
    {
        rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.hideFlags = HideFlags.HideInInspector;
        rigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        rigidbody.isKinematic = true;
        rigidbody.detectCollisions = false;

        capsuleCollider = GetComponent<CapsuleCollider>();
    }

    private void FixedUpdate()
    {
        Velocity += Physics.gravity * Time.fixedDeltaTime;

        CheckGrounded();

        rigidbody.position += Velocity * Time.fixedDeltaTime;

        if (Velocity.x < -maximumMoveSpeed)
        {
            Velocity = new Vector3(-maximumMoveSpeed, Velocity.y, Velocity.z);
        }

        if (Velocity.x > maximumMoveSpeed)
        {
            Velocity = new Vector3(maximumMoveSpeed, Velocity.y, Velocity.z);
        }
    }

    public void Move(float speed)
    {
        Velocity += new Vector3(speed, 0f, 0f);
    }

    public void Jump(float force)
    {
        Velocity = new Vector3(Velocity.x, force, Velocity.z);
    }

    private void CheckGrounded()
    {
        float radius = capsuleCollider.radius;
        Vector3 start = rigidbody.position + capsuleCollider.center + new Vector3(0f, capsuleCollider.height / 2f - radius, 0f);
        Vector3 end = rigidbody.position + capsuleCollider.center - new Vector3(0f, capsuleCollider.height / 2f - radius, 0f);
        Collider[] colliders = VisiblePhysics.OverlapCapsule(start, end, capsuleCollider.radius).Where(c => c.transform != transform).ToArray();

        Vector3 penetration = Vector3.zero;
        foreach (var collider in colliders)
        {
            Vector3 direction;
            float distance;
            if (Physics.ComputePenetration(
                capsuleCollider, rigidbody.position, rigidbody.rotation,
                collider, collider.transform.position, collider.transform.rotation,
                out direction, out distance))
            {
                penetration += direction * distance;
            }
        }

        rigidbody.position += penetration;
        Velocity = Vector3.ProjectOnPlane(Velocity, penetration);
    }
}
