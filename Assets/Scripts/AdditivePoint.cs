﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStack;

public class AdditivePoint : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer = null;
    [SerializeField] private TextMesh textMesh = null;

    public void Setup(int point)
    {
        if (point <= 1)
            textMesh.text = "";
        else
            textMesh.text = $"×{point}";
    }

    private void Start()
    {
        transform.TweenMoveDelta(new Vector3(0f, 0.5f, 0f), 0.1f);
        Destroy(gameObject, 1f);
    }

    private void Update()
    {
        spriteRenderer.transform.eulerAngles += new Vector3(0f, 1000f * Time.deltaTime, 0f);
    }
}
