﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterRigidbody2D))]
public class Enemy1 : MonoBehaviour
{
    private CharacterRigidbody2D characterBody = null;
    [SerializeField, HideInInspector] private float speed = -0.3f;
    [SerializeField, HideInInspector] private bool isMovable = false;

    private void Start()
    {
        characterBody = GetComponent<CharacterRigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (Camera.main.transform.position.x - (Camera.main.orthographicSize * Camera.main.aspect + 1f) < transform.position.x &&
            Camera.main.transform.position.x + (Camera.main.orthographicSize * Camera.main.aspect + 1f) > transform.position.x)
        {
            isMovable = true;
        }

        if (characterBody.isLeftToutched) speed = Mathf.Abs(speed);
        if (characterBody.isRightToutched) speed = -Mathf.Abs(speed);

        if (isMovable)
            characterBody.Move(speed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        foreach (var contact in collision.contacts)
        {
            if (contact.collider.gameObject.layer == LayerMask.NameToLayer("Player") &&
                contact.normal.y < -Mathf.Cos(45f * Mathf.Deg2Rad))
            {
                Destroy(gameObject);
                Rigidbody2D temp = contact.collider.GetComponent<Rigidbody2D>();
                temp.velocity = new Vector2(temp.velocity.x, 10f);
            }
        }
    }
}
