﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private Transform player = null;

    private void Start()
    {
        float horizontalSize = Camera.main.orthographicSize * Camera.main.aspect;
        float verticalSize = Camera.main.orthographicSize;

        Camera.main.transform.position = new Vector3(horizontalSize, verticalSize, -10f);
    }

    private void Update()
    {
        float horizontalSize = Camera.main.orthographicSize * Camera.main.aspect;
        float verticalSize = Camera.main.orthographicSize;

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(
            Mathf.Max(horizontalSize, player.position.x),
            Mathf.Max(verticalSize, player.position.y),
            -10f
        ), 0.1f);
    }
}
